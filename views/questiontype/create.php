<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Questiontype */

$this->title = Yii::t('app', 'Create Questiontype');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questiontypes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questiontype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
