<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "questiontype".
 *
 * @property int $id
 * @property string $questionTypeValue
 */
class Questiontype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questiontype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionTypeValue'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionTypeValue' => 'Question Type Value',
        ];
    }
}
