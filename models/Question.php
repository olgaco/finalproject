<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property string $explenation
 * @property int $sectorId
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $status
 * @property int $show
 * @property string $body
 * @property int $type
 * @property string $help
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['explenation', 'body'], 'string'],
            [['sectorId', 'created_at', 'updated_at', 'created_by', 'updated_by', 'show', 'type'], 'integer'],
            [['status', 'help'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'explenation' => Yii::t('app', 'Explenation'),
            'sectorId' => Yii::t('app', 'Sector ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'show' => Yii::t('app', 'Show'),
            'body' => Yii::t('app', 'Body'),
            'type' => Yii::t('app', 'Type'),
            'help' => Yii::t('app', 'Help'),
        ];
    }
}
