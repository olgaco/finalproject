<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property string $avatar
 * @property string $ficFirstName
 * @property string $ficLastName
 * @property int $userId
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'integer'],
            [['avatar', 'ficFirstName', 'ficLastName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avatar' => 'Avatar',
            'ficFirstName' => 'Fic First Name',
            'ficLastName' => 'Fic Last Name',
            'userId' => 'User ID',
        ];
    }
}
