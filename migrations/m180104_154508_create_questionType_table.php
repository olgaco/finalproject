<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questionType`.
 */
class m180104_154508_create_questionType_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('questionType', [
            'id' => $this->primaryKey(),
            'questionTypeValue' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('questionType');
    }
}
