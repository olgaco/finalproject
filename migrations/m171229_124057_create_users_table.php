<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171229_124057_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'password' => $this->string(),
            'auth_Key' => $this->string(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'phoneNumber' => $this->integer(),
            'address' => $this->string(),
            'profileId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
